# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction

from ..census_ws import CensusWS


__all__ = ['CensusDataStart', 'CensusData']


class CensusDataStart(ModelView):
    'Census Data Start'
    __name__ = 'party.census_data.start'

    identificado_renaper = fields.Char('RENAPER code', readonly=True)
    padron_sisa = fields.Char('SISA census', readonly=True)
    codigo_sisa = fields.Char('SISA code', readonly=True)
    tipo_documento = fields.Char('ID type', readonly=True)
    nro_documento = fields.Char('ID number', readonly=True)
    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    sexo = fields.Char('Sex', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)
    estado_civil = fields.Char('Marital status', readonly=True)
    provincia = fields.Char('Province', readonly=True)
    departamento = fields.Char('Subdivision', readonly=True)
    localidad = fields.Char('City', readonly=True)
    domicilio = fields.Char('Address', readonly=True)
    piso_depto = fields.Char('Apartment', readonly=True)
    codigo_postal = fields.Char('Zip code', readonly=True)
    pais_nacimiento = fields.Char('Birth country', readonly=True)
    provincia_nacimiento = fields.Char('Birth province', readonly=True)
    localidad_nacimiento = fields.Char('Birth city', readonly=True)
    nacionalidad = fields.Char('Nationality', readonly=True)
    fallecido = fields.Char('Deceased', readonly=True)
    tipo_cobertura = fields.Char('Insurance type', readonly=True)
    obra_social = fields.Char('Insurance party', readonly=True)
    rnos = fields.Char('RNOS code', readonly=True)


class CensusData(Wizard):
    'Census Data'
    __name__ = 'party.census_data'

    @classmethod
    def __setup__(cls):
        super(CensusData, cls).__setup__()
        cls._error_messages.update({
            'error_ws': 'SISA web service error',
            'error_autenticacion': 'User authentication error',
            'error_inesperado': 'Unexpected error',
            'no_cuota_disponible': 'User has no asigned quota',
            'error_datos': 'Remote call error',
            'registro_no_encontrado': 'Citizen not found in RENAPER',
            'servicio_no_disponible': 'RENAPER service unavailable',
            'multiple_resultado': 'More than one result, '
            'please set the gender and then try again',
            'unknown_error': 'Unknown error',
        })

    start = StateView(
        'party.census_data.start',
        'health_sisa_census.census_data_start_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'update_party', 'tryton-ok', default=True),
        ])
    update_party = StateTransition()

    def default_start(self, fields):
        Party = Pool().get('party.party')

        res = {}
        party = Party(Transaction().context['active_id'])
        if not party:
            return res
        xml = CensusWS.get_xml(party.ref, party.gender)
        if not xml:
            self.raise_user_error('error_ws')
        if xml.findtext('resultado') == 'OK':
            res = {
                'identificado_renaper':
                    xml.findtext('identificadoRenaper'),
                'padron_sisa': xml.findtext('PadronSISA'),
                'codigo_sisa': xml.findtext('codigoSISA'),
                'tipo_documento': xml.findtext('tipoDocumento'),
                'nro_documento': xml.findtext('nroDocumento'),
                'apellido': xml.findtext('apellido'),
                'nombre': xml.findtext('nombre'),
                'sexo': xml.findtext('sexo'),
                'fecha_nacimiento': xml.findtext('fechaNacimiento'),
                'estado_civil': xml.findtext('estadoCivil'),
                'provincia': xml.findtext('provincia'),
                'departamento': xml.findtext('departamento'),
                'localidad': xml.findtext('localidad'),
                'domicilio': xml.findtext('domicilio'),
                'piso_depto': xml.findtext('pisoDpto'),
                'codigo_postal': xml.findtext('codigoPostal'),
                'pais_nacimiento': xml.findtext('paisNacimiento'),
                'provincia_nacimiento':
                    xml.findtext('provinciaNacimiento'),
                'localidad_nacimiento':
                    xml.findtext('localidadNacimiento'),
                'nacionalidad': xml.findtext('nacionalidad'),
                'fallecido': xml.findtext('fallecido'),
                'tipo_cobertura': xml.findtext('tipoCoberturaVigente'),
                'obra_social': xml.findtext('obraSocialVigente'),
                'rnos': xml.findtext('rnos'),
            }
        elif xml.findtext('resultado') == 'ERROR_AUTENTICACION':
            self.raise_user_error('error_autenticacion')
        elif xml.findtext('resultado') == 'ERROR_INESPERADO':
            self.raise_user_error('error_inesperado')
        elif xml.findtext('resultado') == 'NO_TIENE_QUOTA_DISPONIBLE':
            self.raise_user_error('no_cuota_disponible')
        elif xml.findtext('resultado') == 'ERROR_DATOS':
            self.raise_user_error('error_datos')
        elif xml.findtext('resultado') == 'REGISTRO_NO_ENCONTRADO':
            self.raise_user_error('registro_no_encontrado')
        elif xml.findtext('resultado') == 'SERVICIO_RENAPER_NO_DISPONIBLE':
            self.raise_user_error('servicio_no_disponible')
        elif xml.findtext('resultado') == 'MULTIPLE_RESULTADO':
            self.raise_user_error('multiple_resultado')
        else:
            self.raise_user_error('unknown_error')

        return res

    def transition_update_party(self):
        pool = Pool()
        Party = pool.get('party.party')
        Identifier = pool.get('party.identifier')
        Address = pool.get('party.address')
        Country = pool.get('country.country')
        Insurance = pool.get('gnuhealth.insurance')

        party_id = Transaction().context.get('active_id')
        party = Party(party_id)
        party.name = self.start.nombre
        party.lastname = self.start.apellido
        party.ref = self.start.nro_documento
        # Force official Person Name update
        Party.update_person_official_name(party.id, self.start.nombre,
            self.start.apellido)
        if self.start.fecha_nacimiento:
            party.dob = self.start.fecha_nacimiento
        if self.start.sexo:
            party.gender = self.start.sexo.lower()
        if self.start.pais_nacimiento:
            country = Country.search(
                ['name', 'ilike', self.start.pais_nacimiento]
                )
            if country:
                party.citizenship = country[0].id

        doc_type = 'ar_dni' if self.start.tipo_documento == 'DNI' else None
        identifier = Identifier.search([
            ('party', '=', party.id),
            ('type', '=', doc_type),
            ('code', '=', self.start.nro_documento),
            ])
        if not identifier:
            Identifier.create([{
                'party': party.id,
                'type': doc_type,
                'code': self.start.nro_documento,
                }])

        if self.start.domicilio:
            direccion = Address().search([
                ('party', '=', party.id),
                ])
            if direccion and (direccion[0].street is None
                    or direccion[0].street == ''):
                direccion[0].update_direccion(party, self.start)
            else:
                direccion = Address()
                direccion.update_direccion(party, self.start)

        if self.start.rnos and self.start.obra_social:
            insurance_party = Party().search([
                ('is_insurance_company', '=', True),
                ('identifiers.type', '=', 'ar_rnos'),
                ('identifiers.code', '=', self.start.rnos),
                ])
            if not insurance_party:
                insurance_party = Party().search([
                    ('is_insurance_company', '=', True),
                    ('name', '=', self.start.obra_social),
                    ])

            if insurance_party:
                insurance = Insurance().search([
                    ('name', '=', party_id),
                    ('company', '=', insurance_party[0].id),
                    ])
                if not insurance:
                    insurance_data = {
                        'name': party_id,
                        'number': self.start.nro_documento,
                        'company': insurance_party[0].id,
                        'insurance_type':
                            insurance_party[0].insurance_company_type,
                        }
                    Insurance.create([insurance_data])

        party.identified_renaper = True
        party.renaper_id = self.start.identificado_renaper
        if self.start.padron_sisa == 'SI':
            party.identified_sisa = True
            party.sisa_code = self.start.codigo_sisa
        party.save()
        return 'end'

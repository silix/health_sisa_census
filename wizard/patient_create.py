# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
from datetime import datetime

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, \
    Button
from trytond.pool import Pool

from ..census_ws import CensusWS


__all__ = ['PatientCreateStart', 'PatientCreateFound', 'PatientCreateNotFound',
    'PatientCreateExistent', 'PatientCreateManual', 'PatientCreateData',
    'PatientCreate']


class PatientCreateStart(ModelView):
    'Patient Create Start'
    __name__ = 'gnuhealth.patient.create.start'

    nro_documento = fields.Char('ID number')
    sexo = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], 'Gender')


class PatientCreateFound(ModelView):
    'Patient Create Found'
    __name__ = 'gnuhealth.patient.create.found'

    identificado_renaper = fields.Char('RENAPER code', readonly=True)
    padron_sisa = fields.Char('SISA census', readonly=True)
    codigo_sisa = fields.Char('SISA code', readonly=True)
    tipo_documento = fields.Char('ID type', readonly=True)
    nro_documento = fields.Char('ID number', readonly=True)
    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    sexo = fields.Char('Sex', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)
    estado_civil = fields.Char('Marital status', readonly=True)
    provincia = fields.Char('Province', readonly=True)
    departamento = fields.Char('Subdivision', readonly=True)
    localidad = fields.Char('City', readonly=True)
    domicilio = fields.Char('Address', readonly=True)
    piso_depto = fields.Char('Apartment', readonly=True)
    codigo_postal = fields.Char('Zip code', readonly=True)
    pais_nacimiento = fields.Char('Birth country', readonly=True)
    provincia_nacimiento = fields.Char('Birth province', readonly=True)
    localidad_nacimiento = fields.Char('Birth city', readonly=True)
    nacionalidad = fields.Char('Nationality', readonly=True)
    fallecido = fields.Char('Deceased', readonly=True)
    tipo_cobertura = fields.Char('Insurance type', readonly=True)
    obra_social = fields.Char('Insurance party', readonly=True)
    rnos = fields.Char('RNOS code', readonly=True)


class PatientCreateNotFound(ModelView):
    'Patient Create Not Found'
    __name__ = 'gnuhealth.patient.create.not_found'

    result = fields.Text('Result', readonly=True)


class PatientCreateExistent(ModelView):
    'Patient Create Existent'
    __name__ = 'gnuhealth.patient.create.existent'

    info = fields.Text('Info', readonly=True)


class PatientCreateManual(ModelView):
    'Patient Create Manual'
    __name__ = 'gnuhealth.patient.create.manual'

    apellido = fields.Char('Lastname', required=True)
    nombre = fields.Char('Name', required=True)
    nro_documento = fields.Char('ID number')
    sexo = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], 'Gender', required=True)
    fecha_nacimiento = fields.Date('Birth date')
    scanned_id = fields.Boolean('Scanner used to read ID')

    @fields.depends('apellido')
    def on_change_apellido(self):
        '''
        Example ID to be parsed:
        00305133441"MIAPELLIDO"MI NOMBRE"M"20123456"A"20-12-1950"02-10-2014"207
        '''
        code = self.apellido
        if len(code) > 60:
            elements = code.split('"')
            self.apellido = elements[1]
            self.nombre = elements[2]
            self.nro_documento = elements[4]
            self.sexo = elements[3].lower()
            self.fecha_nacimiento = datetime.strptime(elements[6], '%d-%m-%Y')
            self.scanned_id = True


class PatientCreateData(ModelView):
    'Patient Create Data'
    __name__ = 'gnuhealth.patient.create.data'

    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    nro_documento = fields.Char('ID number', readonly=True)
    sexo = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], 'Gender', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)


class PatientCreate(Wizard):
    'Patient Create'
    __name__ = 'gnuhealth.patient.create'

    start_state = 'start'
    start = StateView('gnuhealth.patient.create.start',
        'health_sisa_census.view_patient_create_start', [
            Button('Check census', 'check_sisa', 'tryton-connect',
                default=True),
            Button('Manual creation', 'manual', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    check_sisa = StateTransition()
    found = StateView('gnuhealth.patient.create.found',
        'health_sisa_census.view_patient_create_found', [
            Button('Create patient', 'create_patient', 'tryton-ok',
                default=True),
            Button('Search again', 'start', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    not_found = StateView('gnuhealth.patient.create.not_found',
        'health_sisa_census.view_patient_create_not_found', [
            Button('Search again', 'start', 'tryton-ok', default=True),
            Button('Manual creation', 'manual', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    existent = StateView('gnuhealth.patient.create.existent',
        'health_sisa_census.view_patient_create_existent', [
            Button('Search again', 'start', 'tryton-ok', default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    manual = StateView('gnuhealth.patient.create.manual',
        'health_sisa_census.view_patient_create_manual', [
            Button('Create patient', 'show_data', 'tryton-ok',
                default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    show_data = StateTransition()
    data = StateView('gnuhealth.patient.create.data',
        'health_sisa_census.view_patient_create_data', [
            Button('Create patient', 'create_manually', 'tryton-ok',
                default=True),
            Button('Search again', 'manual', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    create_patient = StateAction('health.action_gnuhealth_patient_view')
    create_manually = StateAction('health.action_gnuhealth_patient_view')

    def transition_check_sisa(self):
        Party = Pool().get('party.party')

        id_number = self.start.nro_documento
        sex = self.start.sexo
        if not id_number:
            self.not_found.result = 'No ha indicado número de documento'
            return 'not_found'
        party = Party.search(['ref', '=', id_number])
        if party:
            self.existent.info = 'Ya existe una persona identificada ' \
                'con el número %s: %s, %s' % (id_number, party[0].lastname,
                    party[0].name)
            return 'existent'

        xml = CensusWS.get_xml(id_number, sex)
        if xml is None:
            self.not_found.result = 'Error en consulta a webservice'
            return 'not_found'
        if xml.findtext('resultado') == 'OK':
            self.found.identificado_renaper = \
                xml.findtext('identificadoRenaper')
            self.found.padron_sisa = xml.findtext('PadronSISA')
            self.found.codigo_sisa = xml.findtext('codigoSISA')
            self.found.tipo_documento = xml.findtext('tipoDocumento')
            self.found.nro_documento = xml.findtext('nroDocumento')
            self.found.apellido = xml.findtext('apellido')
            self.found.nombre = xml.findtext('nombre')
            self.found.sexo = xml.findtext('sexo')
            self.found.fecha_nacimiento = xml.findtext('fechaNacimiento')
            self.found.estado_civil = xml.findtext('estadoCivil')
            self.found.provincia = xml.findtext('provincia')
            self.found.departamento = xml.findtext('departamento')
            self.found.localidad = xml.findtext('localidad')
            self.found.domicilio = xml.findtext('domicilio')
            self.found.piso_depto = xml.findtext('pisoDpto')
            self.found.codigo_postal = xml.findtext('codigoPostal')
            self.found.pais_nacimiento = xml.findtext('paisNacimiento')
            self.found.provincia_nacimiento = \
                xml.findtext('provinciaNacimiento')
            self.found.localidad_nacimiento = \
                xml.findtext('localidadNacimiento')
            self.found.nacionalidad = xml.findtext('nacionalidad')
            self.found.fallecido = xml.findtext('fallecido')
            self.found.tipo_cobertura = xml.findtext('tipoCoberturaVigente')
            self.found.obra_social = xml.findtext('obraSocialVigente')
            self.found.rnos = xml.findtext('rnos')
            return 'found'
        else:
            if xml.findtext('resultado') == 'ERROR_AUTENTICACION':
                self.not_found.result = 'Error de autenticación'
            elif xml.findtext('resultado') == 'ERROR_INESPERADO':
                self.not_found.result = 'Error inesperado'
            elif xml.findtext('resultado') == 'NO_TIENE_QUOTA_DISPONIBLE':
                self.not_found.result = 'No tiene cuota de uso disponible'
            elif xml.findtext('resultado') == 'ERROR_DATOS':
                self.not_found.result = 'Error de datos'
            elif xml.findtext('resultado') == 'REGISTRO_NO_ENCONTRADO':
                self.not_found.result = 'Registro no encontrado'
            elif xml.findtext('resultado') == 'SERVICIO_RENAPER_NO_DISPONIBLE':
                self.not_found.result = 'Servicio no disponible'
            elif xml.findtext('resultado') == 'MULTIPLE_RESULTADO':
                self.not_found.result = 'Resultado múltiple: se sugiere ' \
                    'reintentar definiendo el sexo de la persona'
            else:
                self.not_found.result = 'Error desconocido'
            return 'not_found'

    def default_manual(self, fields):
        return {
            'scanned_id': False,
            }

    def default_data(self, fields):
        return {
            'apellido': self.manual.apellido,
            'nombre': self.manual.nombre,
            'nro_documento': self.manual.nro_documento,
            'sexo': self.manual.sexo,
            'fecha_nacimiento': self.manual.fecha_nacimiento,
            }

    def default_found(self, fields):
        return {
            'identificado_renaper': self.found.identificado_renaper,
            'padron_sisa': self.found.padron_sisa,
            'codigo_sisa': self.found.codigo_sisa,
            'tipo_documento': self.found.tipo_documento,
            'nro_documento': self.found.nro_documento,
            'apellido': self.found.apellido,
            'nombre': self.found.nombre,
            'sexo': self.found.sexo,
            'fecha_nacimiento': self.found.fecha_nacimiento,
            'estado_civil': self.found.estado_civil,
            'provincia': self.found.provincia,
            'departamento': self.found.departamento,
            'localidad': self.found.localidad,
            'domicilio': self.found.domicilio,
            'piso_depto': self.found.piso_depto,
            'codigo_postal': self.found.codigo_postal,
            'pais_nacimiento': self.found.pais_nacimiento,
            'provincia_nacimiento': self.found.provincia_nacimiento,
            'localidad_nacimiento': self.found.localidad_nacimiento,
            'nacionalidad': self.found.nacionalidad,
            'fallecido': self.found.fallecido,
            'tipo_cobertura': self.found.tipo_cobertura,
            'obra_social': self.found.obra_social,
            'rnos': self.found.rnos,
            }

    def default_not_found(self, fields):
        return {
            'result': self.not_found.result,
            }

    def default_existent(self, fields):
        return {
            'info': self.existent.info,
            }

    def do_create_patient(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Address = pool.get('party.address')
        Country = pool.get('country.country')
        Insurance = pool.get('gnuhealth.insurance')
        Patient = pool.get('gnuhealth.patient')

        identifier_data = {
            'type': 'ar_dni' if self.found.tipo_documento == 'DNI' else None,
            'code': self.found.nro_documento,
            }

        party_data = {
            'name': self.found.nombre,
            'lastname': self.found.apellido,
            'ref': self.found.nro_documento,
            'identifiers': [('create', [identifier_data])],
            'dob': self.found.fecha_nacimiento,
            'gender': self.found.sexo.lower() if self.found.sexo else 'm',
            'identified_renaper': True,
            'renaper_id': self.found.identificado_renaper,
            'is_person': True,
            'is_patient': True,
            }
        if self.found.padron_sisa == 'SI':
            party_data['identified_sisa'] = True
            party_data['sisa_code'] = self.found.codigo_sisa
        # 'party_ar' module requires vat_number definition
        if 'trytond.modules.party_ar' in sys.modules:
            party_data['iva_condition'] = 'consumidor_final'

        if self.found.pais_nacimiento:
            country = Country.search(
                ['name', 'ilike', self.found.pais_nacimiento]
                )
            if country:
                party_data['citizenship'] = country[0].id

        party = Party.create([party_data])
        patient = None

        if party:
            if self.found.domicilio:
                direccion = Address().search([
                    ('party', '=', party[0].id),
                    ])
                if direccion and (direccion[0].street is None
                        or direccion[0].street == ''):
                    direccion[0].update_direccion(party[0], self.found)
                else:
                    direccion = Address()
                    direccion.update_direccion(party[0], self.found)

            insurance = None
            if self.found.rnos and self.found.obra_social:
                insurance_party = Party().search([
                    ('is_insurance_company', '=', True),
                    ('identifiers.type', '=', 'ar_rnos'),
                    ('identifiers.code', '=', self.found.rnos),
                    ])
                if not insurance_party:
                    insurance_party = Party().search([
                        ('is_insurance_company', '=', True),
                        ('name', '=', self.found.obra_social),
                        ])

                if insurance_party:
                    insurance = Insurance().search([
                        ('name', '=', party[0].id),
                        ('company', '=', insurance_party[0].id),
                        ])
                    if not insurance:
                        insurance_data = {
                            'name': party[0].id,
                            'number': self.found.nro_documento,
                            'company': insurance_party[0].id,
                            'insurance_type':
                                insurance_party[0].insurance_company_type,
                            }
                        insurance = Insurance.create([insurance_data])

            patient = Patient.create([{
                'name': party[0],
                }])
            if patient and insurance:
                if not patient[0].current_insurance:
                    patient[0].current_insurance = insurance[0]
                    patient[0].save()

        data = {}
        if patient:
            data = {'res_id': [p.id for p in patient]}
            if len(patient) == 1:
                action['views'].reverse()
        return action, data

    def transition_show_data(self):
        if self.manual.scanned_id:
            return 'data'
        return 'create_manually'

    def do_create_manually(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')

        identifier_data = {
            'type': 'ar_dni',
            'code': self.manual.nro_documento,
            }

        party_data = {
            'name': self.manual.nombre,
            'lastname': self.manual.apellido,
            'ref': self.manual.nro_documento,
            'identifiers': [('create', [identifier_data])],
            'dob': self.manual.fecha_nacimiento,
            'gender': self.manual.sexo,
            'is_person': True,
            'is_patient': True,
            }
        # 'party_ar' module requires vat_number definition
        if 'trytond.modules.party_ar' in sys.modules:
            party_data['iva_condition'] = 'consumidor_final'

        party = Party.create([party_data])
        if party:
            patient = Patient.create([{
                'name': party[0],
                }])

        data = {'res_id': [p.id for p in patient]}
        if len(patient) == 1:
            action['views'].reverse()
        return action, data
